<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Project/PHP/PHPProject.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Electricity Consumption Calculator</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        .output-box {
            border: 1px solid #ccc;
            padding: 10px;
            margin-bottom: 10px;
        }
        
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid #ccc;
            padding: 10px;
        }
    </style>
    </head>
    <body>
        <div class="container">
            <h1>Electricity Consumption Calculator</h1>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="voltage">Voltage (V):</label>
                    <input type="number" step="any" class="form-control" id="voltage" name="voltage" required>
                </div>
                <div class="form-group">
                    <label for="current">Current (A):</label>
                    <input type="number" step="any" class="form-control" id="current" name="current" required>
                </div>
                <div class="form-group">
                    <label for="rate">Current Rate:</label>
                    <input type="number" step="any" class="form-control" id="rate" name="rate" required>
                </div>
                <button type="submit" class="btn btn-primary">Calculate</button>
            </form>
            <br>

            <?php
                function calculateElectricityRates($voltage, $current, $rate) {
                    $results = []; // Initialize an empty array to store the results

                    // Loop for 1 to 24 hours
                    for ($hour = 1; $hour <= 24; $hour++) {
                        // Calculate power (W)
                        $power = ($voltage * $current);

                        // Calculate energy (kWh)
                        $energy = $power * $hour / 1000;

                        // Calculate total charge per hour
                        $totalChargePerHour = $energy * ($rate / 100);

                        // Store the calculated rates for the current hour
                        $results[] = [
                            'id' => $hour,
                            'hour' => $hour,
                            'power' => number_format($energy, 5, '.', ''),
                            'rate' => number_format($totalChargePerHour, 2, '.', '') . ' RM'
                        ];
                    }

                    return $results; // Return the array of calculated rates for each hour
                }

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    // Get user input values
                    $voltage = $_POST['voltage'];
                    $current = $_POST['current'];
                    $rate = $_POST['rate'];

                    // Calculate rates using the function
                    $rates = calculateElectricityRates($voltage, $current, $rate);

                    // Display the calculated rates for the first hour
                    $firstHourPower = $rates[0]['power'];
                    $firstHourRate = $rate / 100;

                    echo "<div class='output-box'>";
                    echo "<h2>Results:</h2>";
                    echo "<p>Power: " . $firstHourPower . " kW</p>";
                    echo "<p>Rate: " . $firstHourRate . " RM</p>";
                    echo "</div>";
                }
            ?>

            <?php if (isset($rates)): ?>
                <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Hour</th>
                            <th>Power (kW)</th>
                            <th>Total (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($rates as $rate): ?>
                            <tr>
                                <td><?php echo $rate['id']; ?></td>
                                <td><?php echo $rate['hour']; ?></td>
                                <td><?php echo $rate['power']; ?></td>
                                <td><?php echo $rate['rate']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </body>
</html>
